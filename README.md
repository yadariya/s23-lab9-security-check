# Lab9 -- Security check


## Introduction

You will or you already have been creating applications that are rather sensitive to security so you want to check that your application is secure enough. And maybe you think that you are not competent enough for security testing or maybe the cost of failure is rather big in your case, but usually you want to delegate this work to the other institution to let them say whether the system is secure enough. And in this lab, you would be that institution. ***Let's roll!***

## OWASP

[OWASP](https://owasp.org/) is Open Web Application Security Project. It is noncommercial organization that works to improve the security of software. It creates several products and tools, which we will use today and I hope you will at least take into account in future.

## OWASP Top 10

[OWASP Top 10](https://owasp.org/www-project-top-ten/) is a project that highlights 10 current biggest risks in software security. Highlighting that they make it visible. And forewarned is forearmed. As a software developer, you may at least consider that risks.

## OWASP Cheat Sheet Series

[OWASP CSS](https://cheatsheetseries.owasp.org/) is a project that accumulates the information about security splitted into very specific topics. And sometimes that might answer the question "How to make it secure?" according to specific topics (e.g. forgot password functionality).

## Google security check

Let's check how Google responds to the OWASP Cheatsheets of Forgot Password topic.

### Select the requirement from cheatsheets

E.g. how Google provides the forgot password feature and whether it "Return a consistent message for both existent and non-existent accounts."([link](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=forgot%20password%20service%3A-,Return%20a%20consistent%20message%20for%20both%20existent%20and%20non%2Dexistent%20accounts.,-Ensure%20that%20the)). To check this we should only try to reset password for existent and unexistent user and compare result.

Note: _In your work is important to make link to particlular requirement in cheatsheet._

### Create and execute test case

Up to this moment we do not know what would be during testing forgot password feature at google.com. Not only real result, but even steps. But we can expect some steps in this process and some expected result:
1. Beforehand we have to know some existent account. 
2. Open google.com page
3. Try to authenticate
4. Understand that we forgot the password
5. Try to reset password for some unexistent account
6. Remember the result
7. Go back and make the same for the prepared account
8. Remember the result 
9. Expect that results for 2 users would be indistinguishable, or the intruder might process [user enumeration atack](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=user%20enumeration%20attack)

Note: _You should not provide this high level steps, you might take it into account and go to next step to documenting test case execution_

Now lets try to execute that with documenting each step, to let yourself or someone another to reproduce it. During execution some unexpected issues might happen. It is ok, if the planned test case is not changd a lot just mention it into result section. Or if original plan changes a lot then return to it, review it, change if needed and continue.

| Test step                                                                     | Result                                                                                                                                                                                                                        |
|-------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Open google.com                                                               | Ok                                                                                                                                                                                                                            |
| Push sign in button                                                           | Login page opened. If you have already logged in log out and repeate                                                                                                                                                          |
| Push forgot email                                                             | There is no forgot password, but we can push forgot email. It still starts password recovery                                                                                                                                  |
| Open new tab with sign in page                                                | I tried to input unexistent account and understand that i do not know whether the account exists. So I would try to register with new account without submiting the form, so i would be sure about email that does not exists |
| Push create account button                                                    | Ok                                                                                                                                                                                                                            |
| Push create for yourself tab                                                  | Ok, create account form opened                                                                                                                                                                                                |
| Input your existent account email into email field                            | Ok, field highlight red                                                                                                                                                                                                       |
| Try to change this email to make it unique                                    | Ok, also you can input some name and page offer you new unique email. Remember that email                                                                                                                                     |
| Return to forgot password page and input the unexistent email and push submit | Ok, form with first and last names opened                                                                                                                                                                                     |
| Input some first and last names and push submit                               | Since you do not know the names of unexistent account it is ok to imagine somehting                                                                                                                                           |
| See the result that there is no such an account                               | Ok, remember this result                                                                                                                                                                                                      |
| Push retry button                                                             | Ok, we in the start of forgot password process                                                                                                                                                                                |
| Input prepared existent email and push submit                                 | Ok, form with last and first name opened                                                                                                                                                                                      |
| Input first and last names and push submit                                    | Ok, since it is prepared account you know its first and last names                                                                                                                                                            |
| See the page with confirmation  to send resetting email                       | Ok. Remember this result                                                                                                                                                                                                      |

Thats all, we tried 2 accounts and get 2 results, they are different so test case failed

### Analysis

Test case was failed, and according to different result intruders might process user enumeration attack and get database with existing users emails names and surnames, at least there is such an opportunity. Of course to brute force every email it require a lot of resources, but in this case it requires much more, because with email intruder need to pick up its name and surname. So even test case failed, that is not an security issue in given context.

Interesting thing that for my account, the email with resetting information would be sent on the same email(that i want to restore password)

Note: _Do not consider this result analysis as guide to action. Just see the test case result and provide your thoughts about_

## Homework

### Goal

To get familiar with OWASP CSS project by practicing limited security blackbox testing based on its materials. The default topics for testing is "Forgot password" and "Files Upload", but we are appeciate you to read another topics and select interesting one.

Note: _You are free to select topics or even combine topics, but remember that you should be able to test it using black box testing_

### Task

1. Pick a website that you can test using blackbox mode and put it into your solution and check that it is unique with previous submissions(web site and topic tested should be unique toghether, e.g. google.com might be tested on forgot password in one submission, and on file upload in another one). Like it was during [previous lab](https://gitlab.com/sqr-inno/s23-lab8-exploratory-testing-and-ui-testing)(If it is hard for you to find such website just google "Forgot password" and you will see list of forgot password pages, just select one for yourself, or if you select another topic, then google it to find websites and pages with another features)
2. Find specific advice/requirement/standard or similar that you want to check whether your picked website follows it. By default look into ["Forgot password"](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html) and ["File upload"](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html) topics, because there are very straight forward checklists, with possible black box testing. 

Note: _It is not mandatory to test Forgot Password feature, moreover it is strongly recommended to go through cheat sheet and find interesting features to test. Lab guide you for Forgot Password feature, only because I want to let every student to pass this lab. You are free(And I appreciate that) to select any topics (Be sure that you are able to ensure that during black box testing)_

3. For entity from previous stage imagine the flow of test(you do not need to document it, but you still can do it for
   more realism), its inputs, high level steps, and expected results.
4. Follow that flow with documenting each step and results
5. Comment the results of test cases. Is that everything ok, maybe it is a found vulnerability or weakness, or it might
   be intended weaking of the security protection in given context, what might be an alternative solution.(This is not a
   checklist that you must follow, just take it into account as line of thought)
6. Repeat from the first point(actually the website might be the same with different topic or entity from second point,
   and you do not need to mention web site each time) 4 times(the one from the lab is not counted).

### Notes

1. Test cases might be executed fully manually, using helper tools, or fully automated, but in this cases I prefer to
   see the source code or know the used tool.
2. I want to see the linkage between entities from points 2,3,4,5 and with the referenced place from OWASP CSS(hope the
   structure would not change between your submission and my evaluation)
3. Some advices might be easy to test(e.g. password form should contain minimum password length validation), but some of
   them might require a lof of efforts(e.g. Ensure that the time taken for the user response message is uniform for
   existent user and unexistent). So 4 tests is just a recomendation it might be higher or lower. Finnaly I can reject
   submission if see too less effort put into the work. So to write me directly to prereview selected topics and
   proposed flows might be good idea(but not mandatory).

## Solution

Website: https://www.oracle.com

### Test case 1: Forget password

https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-cheat-sheet

```
Plan:
1. have an existed account at https://www.oracle.com
2. go to https://profile.oracle.com/myprofile/account/forgot-password.jspx with existed account, write the email, press "Sumbit" button
3. mark the response time for request from Devtools, and mark the response from server for existed account
4. go to https://profile.oracle.com/myprofile/account/forgot-password.jspx with non-existed account, write the email, press "Sumbit" button
5. mark the response time for request from Devtools, and mark the response from server for non-existed account
6. Compare the results of 3. and 5.
```

| Test step                                                                                                                                      | Result                                                                                        |
|------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| Navigate to https://www.oracle.com                                                                                                             | Done                                                                                          |
| Press "Sing in" button                                                                                                                         | Sing-in page https://login.oracle.com/mysso/signon.jsp opened                                 |
| Click on "Need help"                                                                                                                           | Need-help page https://www.oracle.com/corporate/contact/help.html opened                      |
| At https://www.oracle.com/corporate/contact/help.html choose "I forgot my password"                                                            | Done                                                                                          |
| Click on "Forgot your password" button                                                                                                         | Forget-password page https://profile.oracle.com/myprofile/account/forgot-password.jspx opened |
| At https://profile.oracle.com/myprofile/account/forgot-password.jspx type your mail from existing account and press "Submit button"            | Done                                                                                          |
| Go to DevTools and check the response time for the change password query to existed account                                                    | 1.01 s                                                                                        |
| Mark the response from server to existed account                                                                                               | ![img.png](img.png)                                                                           |
| Return to https://profile.oracle.com/myprofile/account/forgot-password.jspx again and type non-existing account mail and press "Submit button" | Done                                                                                          |
| Go to DevTools and check the response time for the change password query to non-existed account                                                | 1.01 s                                                                                        |
| Mark the response from server to non-existed account                                                                                           | ![img_1.png](img_1.png)                                                                       |

#### Analysis:

for both existed and non-existed accounts the response message and response time is the same. That mean
Oracle follows https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-request
rule : "Return a consistent message for both existent and non-existent accounts" and "Ensure that responses return in a
consistent amount of time to prevent an attacker enumerating which accounts exist"

Result: `Success`. Oracle passed Forgot Password Cheat Sheet test!

### Test case 2: Authentication, User-id section

https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids

```
Plan:
1. have an existed account at https://www.oracle.com or create new one
2. go to https://login.oracle.com/mysso/signon.jsp and type your username with different cases from you type originally
3. the system should let you in 
```

| Test step                                                        | Result                                                                                           |
|------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| Navigate to https://www.oracle.com                               | Done                                                                                             |
| Press "Sing in" button                                           | Sing-in page https://login.oracle.com/mysso/signon.jsp opened                                    |
| type your username with different cases from you type originally | With case-changes typed username `YaDariyaaa` system logs-in, original username was `yadariyaaa` |

#### Analysis:

According to https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids
"Usernames/user IDs are case-insensitive. User 'smith' and user 'Smith' should be the same user. Usernames should also
be unique."
Oracle passes this rule,

Result: `Success`. Oracle passed Authentication Cheat Sheet user-Id test!

### Test case 3: Input Validation, Email Address Syntactic Validation case

https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html#email-address-validation

```
Plan:
1. go on https://www.oracle.com to  create new account
2. fill all required fields excluding email
3. try to fill email more than 254 character
4. try to fill email local part more than 63 character
5. in domain part of email try to use characters other than letters, numbers, hyphens (-) and periods (.)
6. in email try to use more than one @
7. fill email not follwing 3. 4. 5. 6.
```

| Test step                                                                                           | Result                                                                                      |
|-----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| Navigate to https://www.oracle.com                                                                  | Done                                                                                        |
| Press "Create an Account" button                                                                    | Create-account page https://profile.oracle.com/myprofile/account/create-account.jspx opened |
| Fill all required fields excluding email                                                            | Done                                                                                        |
| Fill email field with more then one `@`                                                             | ![img_2.png](img_2.png)                                                                     |
| Fill email field with more than 254 character                                                       | ![img_3.png](img_3.png)                                                                     |
| Fill email local part more than 63 character                                                        | ![img_4.png](img_4.png)                                                                     |
| Fill email in domain part using characters other than letters, numbers, hyphens (-) and periods (.) | ![img_5.png](img_5.png)                                                                     |
| Fill correct email not follwing violation above, like `yadariyaa@mail.ru`                           | ![img_6.png](img_6.png)                                                                     |

#### Analysis:

According to https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html#syntactic-validation
Oracle passes all rules of syntactic check.
The email address contains two parts, separated with an @ symbol.
The email address does not contain dangerous characters (such as backticks, single or double quotes, or null bytes).
Exactly which characters are dangerous will depend on how the address is going to be used (echoed in page, inserted into
database, etc).
The domain part contains only letters, numbers, hyphens (-) and periods (.).
The email address is a reasonable length:
The local part (before the @) should be no more than 63 characters.
The total length should be no more than 254 characters.

Result: `Success`. Oracle passed Email Address Syntactic Validation test!

### Test case 4: HTTP Security Response Headers Cheat Sheet

https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html

```
Plan:
1. go to https://www.oracle.com   
2. make random request, for example go to https://login.oracle.com/mysso/signon.jsp
3. check security header X-Frame-Options
4. check security header X-XSS-Protection
5. check security header X-Content-Type-Options
6. check security header Referrer-Policy
7. check security header Content-Type
8. check security header Set-Cookie
```

| Test step                                                           | Result                    |
|---------------------------------------------------------------------|---------------------------|
| Navigate to https://www.oracle.com                                  | Done                      |
| Open Dev Tools to see the `network` panel                           | Done                      |
| Navigate to https://login.oracle.com/mysso/signon.jsp to do request | Done                      |
| Check security header X-Frame-Options                               | ![img_8.png](img_8.png)   |
| Check security header X-XSS-Protection                              | ![img_7.png](img_7.png)   |
| Check security header X-Content-Type-Options                        | Not provided              |
| Check security header Referrer-Policy                               | Not provided              |
| Check security header Content-Type                                  | ![img_9.png](img_9.png)   |
| Check security header Set-Cookie                                    | ![img_10.png](img_10.png) |

#### Analysis:

According to https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html
Oracle passes all rules of syntactic check.

https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html#x-frame-options
Use Content Security Policy (CSP) frame-ancestors directive if possible. - Oracle uses `sameorigin`

https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html#x-xss-protection
Do not set this header or explicitly turn it off - Oracle does not turn it off

https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html#content-type
Recommendation Content-Type: text/html; charset=UTF-8 - Oracle follows the recommendations

Result: `Success`. HTTP Security Response Headers Cheat Sheet test!

### Result:

Oracle passed 4/4 tests! We can say that this website is quite reliable. 